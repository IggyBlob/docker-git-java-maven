FROM docker:19.03.12-git

# install Zulu Java 17
# https://docs.azul.com/core/zulu-openjdk/install/alpine-linux
RUN wget -P /etc/apk/keys/ https://cdn.azul.com/public_keys/alpine-signing@azul.com-5d5dc44c.rsa.pub
RUN echo "https://repos.azul.com/zulu/alpine" | tee -a /etc/apk/repositories
RUN apk update
RUN apk add zulu17-jdk
ENV JAVA_HOME /usr/lib/jvm/zulu17-ca

# install Maven 3.9.5
# https://www.digitalocean.com/community/tutorials/install-maven-linux-ubuntu
RUN wget -P /tmp/ https://dlcdn.apache.org/maven/maven-3/3.9.5/binaries/apache-maven-3.9.5-bin.tar.gz
RUN tar -xvf /tmp/apache-maven-3.9.5-bin.tar.gz -C /tmp
RUN mv /tmp/apache-maven-3.9.5 /opt
ENV M2_HOME /opt/apache-maven-3.9.5
ENV PATH="${M2_HOME}/bin:${PATH}"
